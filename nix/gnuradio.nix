{ pkgs ? import <nixpkgs> {} }:

pkgs.gnuradio.override {
  extraPythonPackages = [
    # add python libs to use inside GNU Radio here
  ];
  extraPackages = [
    (pkgs.callPackage ./librelora.nix {})
    # add other GNU Radio block libraries here
  ];
}

